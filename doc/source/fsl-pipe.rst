fsl\_pipe package
=================

.. automodule:: fsl_pipe
   :members:
   :undoc-members:
   :show-inheritance:

fsl\_pipe.pipeline module
-------------------------

.. automodule:: fsl_pipe.pipeline
   :members:
   :undoc-members:
   :show-inheritance:

fsl\_pipe.job module
--------------------

.. automodule:: fsl_pipe.job
   :members:
   :undoc-members:
   :show-inheritance:

fsl\_pipe.datalad module
------------------------

.. automodule:: fsl_pipe.datalad
   :members:
   :undoc-members:
   :show-inheritance: